from django.contrib.auth import get_user_model
from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from django.conf import settings
User = settings.AUTH_USER_MODEL


class UserDeviceInfo(models.Model):
    # title
    device_name = models.CharField(max_length=100)
    ip_address = models. GenericIPAddressField(null=True)
    created_at = models.DateTimeField(auto_now_add=True)  # created_at

    def __str__(self):
        # return the device_name title
        return self.device_name


class UserDevice(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_device')
    device_name = models.CharField(max_length=255, null=True)
    ip_address = models.CharField(max_length=255, null=True)
    last_login = models.DateTimeField(null=True)

    class Meta:
        verbose_name = 'User Device'
        verbose_name_plural = 'User Devices'

    def __str__(self):
        return f'{self.user.username} - {self.device_name} ({self.ip_address})'

    def save(self, *args, **kwargs):
        # Save the related user object first
        if self.user and not self.user.pk:
            self.user.save()
        super().save(*args, **kwargs)


class FailedLogin(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    timestamp = models.DateTimeField(auto_now_add=True, null=True)
